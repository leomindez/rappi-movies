# Rappi Test

## Arquitectura
	
	La aplicaci�n se construy� utilizando Model-View-Presenter con lo cu�l se ha creado un paquete que contiene las clases base para toda la aplicaci�n
	
	Los paquetes se dividen:
	
	* Paquete data : Contiene las clases que ayudar�n a realizar peticiones HTTP, tambi�n contiene la clase USECASE que ayuda a implementar el caso de uso 

		- NetManager: Pertite realizar peticiones HTTP. Se realiz� con ayuda de FUEL
		- Parser: Funci�n que traduce JSON a Objeto. Se realiz� con ayuda de GSON


	* Paquete presenter: Contiene clase abstracta que ayuda a obtener la vista para mostrar la informaci�n y tambien para realizar la comunicaci�n entre capas

		- Presenter<V :View> Clase gen�rica que contiene una variable de tipo V que ayuda a obtener la instancia de la vista 


	* Paquete view: Contiene interfaz view que ayuda a mostrar los resultados en la interfaz gr�fica

		- View Interfaz donde se implementar�n los m�todos que mostrar�n informaci�n en la interfaz gr�fica


	* Paquete database: Contienen la implementaci�n de la base de datos. Se encuentra un DatabaseManager el cual ayuda a escribir y obtener resultados asicronamente 

		- DatabaseContract: Contiene nombre de columnas, tablas, base de datos y versi�n 
		- SqLiteHelper : Contiene la implementaci�n de la clase SQLiteOpenHelper y donde se realiza la creaci�n de las tablas
		- DatabaseManager: Clase que realiza las operaci�nes de lectura y escritura asincronamente

	


## Principio de responsabilidad �nica

	Prop�sito. Tareas especificas relacionadas con las clases y objetos. Este principio declara que clases implementadas deben de cubrir responsabilidades especificas 
	para lo que esta dise�ada. 
	
	Un ejemplo ser�a tener una interfaz Translate la cual tiene el metodo translateTo(String from, String to). La clase que implementa la interfaz
	debe de cumplir su trabajo el cual es realizar una traducci�n. 


## Caracter�sticas de un c�digo limpio
	
	* Buena l�gica
	* Nombre de m�todos de acuerdo a su prop�sito
	* Nombre de variables de acuerdo a su prop�sito 
	* Divisi�n de capas de acuerdo a su trabajo, ejemplo Modelo, Vista, L�gica
	* Pruebas unitarias
	* Independencia de clases
	* Excelente protocolo de comunicaci�n entre clases 