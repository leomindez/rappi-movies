package mx.leo.testrappi.application

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco

/**
 * Created by mendezl on 15/05/2017.
 */
class App:Application() {
    override fun onCreate() {
        super.onCreate()
        Fresco.initialize(this)
    }
}