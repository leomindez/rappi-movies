package mx.leo.testrappi.base.data.net

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.core.interceptors.cUrlLoggingRequestInterceptor
import com.github.kittinunf.fuel.util.readWriteLazy
import com.github.kittinunf.result.Result
import com.github.kittinunf.result.getAs

/**
 * NetManager class handles http requests and responses by parsing response into object
 */

class NetManager private constructor() {

    companion object {
        var instance = lazy { NetManager() }
    }

    object Config {
        var baseUrl = ""
        var headers:Map<String,String>? = null;
        var baseParams:List<Pair<String,Any?>> = listOf()
    }

    init{
        FuelManager.instance.basePath = Config.baseUrl
        FuelManager.instance.baseParams = Config.baseParams
        FuelManager.instance.baseHeaders = Config.headers
        FuelManager.instance.addRequestInterceptor( cUrlLoggingRequestInterceptor())
    }

    inline fun<reified T> get(endpoint:String, params:List<Pair<String,Any?>>? = null, crossinline success:(T) -> Unit, crossinline error:(Error) -> Unit){
        Fuel.get(endpoint,params).responseString{ request, response, result ->
            when(result) {
                is Result.Failure -> {
                    error(result.error)
                }
                is Result.Success -> {
                    success(parse<T>(result.value))
                }
            }
        }
    }

    fun post(params:Map<Any,Any>? = null){

    }
}