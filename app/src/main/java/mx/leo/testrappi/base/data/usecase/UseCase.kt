package mx.leo.testrappi.base.data.usecase

/**
 * Created by Leo on 13/05/17.
 */
abstract class UseCase<P,R> {
    abstract fun execute(params:P?= null, success:(R) -> Unit, error : (Error) -> Unit)
}