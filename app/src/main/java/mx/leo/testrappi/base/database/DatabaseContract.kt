package mx.leo.testrappi.base.database

import android.provider.BaseColumns

/**
 * Created by mendezl on 15/05/2017.
 */
class DatabaseContract:BaseColumns{
    companion object{
        var DATABASE_MOVIES = "movies.db"
        var DATABASE_VERSION = 1
        var TABLE_TOP_MOVIES = "top_movies"
        var TABLE_RATED_MOVIES = "rated_movies"
        var TABLE_DISCOVERY_MOVIES = "discovery_movies"

        var COLUMN_MOVIE_POSTER_PATH = "movie_poster_path"
        var COLUMN_MOVIE_OVERVIEW = "movie_overview"
        var COLUMN_MOVIE_RELEASE_DATE = "movie_release_date"
        var COLUMN_MOVIE_ID = "movie_id"
        var COLUMN_MOVIE_TITLE = "movie_title"
    }
}