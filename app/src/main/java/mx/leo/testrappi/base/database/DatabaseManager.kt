package mx.leo.testrappi.base.database

import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.ContactsContract
import mx.leo.testrappi.common.model.Movie
import java.util.concurrent.Callable
import java.util.concurrent.Future
import java.util.concurrent.FutureTask

/**
 * Created by mendezl on 15/05/2017.
 */
class DatabaseManager() {
    var sqLiteDatabase: SQLiteOpenHelper? = null

    companion object{
        var instance = lazy { DatabaseManager() }
    }

    fun saveMovies(table:String, movies:ArrayList<Movie>){
        val db = sqLiteDatabase?.writableDatabase

        val saveMoviesAsynchronously = FutureTask<Unit>({
            movies.forEach { movie ->
                val contentValue = ContentValues()
                contentValue.put(DatabaseContract.COLUMN_MOVIE_TITLE,movie.original_title)
                contentValue.put(DatabaseContract.COLUMN_MOVIE_OVERVIEW,movie.overview)
                contentValue.put(DatabaseContract.COLUMN_MOVIE_POSTER_PATH,movie.poster_path)
                contentValue.put(DatabaseContract.COLUMN_MOVIE_RELEASE_DATE, movie.release_date)
                contentValue.put(DatabaseContract.COLUMN_MOVIE_ID,movie.id)
                db?.replace(table,null,contentValue)
            }
        })

        saveMoviesAsynchronously.run()
        if(saveMoviesAsynchronously.isDone)
            db?.close()
    }

    fun getMovies(table:String,callback:(ArrayList<Movie>)->Unit){
        val db = sqLiteDatabase?.readableDatabase

        val future = FutureTask<ArrayList<Movie>>(Callable<ArrayList<Movie>> {

            val values = ArrayList<Movie>()
            val projection = arrayOf<String>(
                    DatabaseContract.COLUMN_MOVIE_ID,
                    DatabaseContract.COLUMN_MOVIE_OVERVIEW,
                    DatabaseContract.COLUMN_MOVIE_POSTER_PATH,
                    DatabaseContract.COLUMN_MOVIE_RELEASE_DATE,
                    DatabaseContract.COLUMN_MOVIE_TITLE
            )

            val cursor = db?.query(table,projection,null,null,null,null,null)
            cursor?.moveToFirst()

            while (cursor!!.moveToNext()){
                values.add(Movie(cursor.getString(cursor.getColumnIndex(DatabaseContract.COLUMN_MOVIE_POSTER_PATH))
                        ,cursor.getString(cursor.getColumnIndex(DatabaseContract.COLUMN_MOVIE_OVERVIEW)),
                        cursor.getString(cursor.getColumnIndex(DatabaseContract.COLUMN_MOVIE_RELEASE_DATE)),
                        cursor.getString(cursor.getColumnIndex(DatabaseContract.COLUMN_MOVIE_ID)),
                        cursor.getString(cursor.getColumnIndex(DatabaseContract.COLUMN_MOVIE_TITLE))
                        ))
            }

            cursor.close()
            values
        })

        future.run()
        if(future.isDone){
            db?.close()
            callback(future.get())
        }
    }
}