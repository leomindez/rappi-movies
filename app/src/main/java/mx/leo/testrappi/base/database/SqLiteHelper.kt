package mx.leo.testrappi.base.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.vansuita.sqliteparser.SqlParser
import org.jetbrains.annotations.Contract

/**
 * Created by mendezl on 15/05/2017.
 */
class SqLiteHelper(context:Context):SQLiteOpenHelper(context,DatabaseContract.DATABASE_MOVIES,null,DatabaseContract.DATABASE_VERSION) {

    private val SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + DatabaseContract.DATABASE_MOVIES

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL(SQL_DELETE_ENTRIES)
        onCreate(db)
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(SqlParser.create(DatabaseContract.TABLE_TOP_MOVIES)
                .pk(DatabaseContract.COLUMN_MOVIE_ID)
                .str(DatabaseContract.COLUMN_MOVIE_TITLE)
                .str(DatabaseContract.COLUMN_MOVIE_RELEASE_DATE)
                .str(DatabaseContract.COLUMN_MOVIE_POSTER_PATH).
                str(DatabaseContract.COLUMN_MOVIE_OVERVIEW).build())

        db?.execSQL(SqlParser.create(DatabaseContract.TABLE_RATED_MOVIES)
                .pk(DatabaseContract.COLUMN_MOVIE_ID)
                .str(DatabaseContract.COLUMN_MOVIE_TITLE)
                .str(DatabaseContract.COLUMN_MOVIE_RELEASE_DATE)
                .str(DatabaseContract.COLUMN_MOVIE_POSTER_PATH).
                str(DatabaseContract.COLUMN_MOVIE_OVERVIEW).build())

        db?.execSQL(SqlParser.create(DatabaseContract.TABLE_DISCOVERY_MOVIES)
                .pk(DatabaseContract.COLUMN_MOVIE_ID)
                .str(DatabaseContract.COLUMN_MOVIE_TITLE)
                .str(DatabaseContract.COLUMN_MOVIE_RELEASE_DATE)
                .str(DatabaseContract.COLUMN_MOVIE_POSTER_PATH).
                str(DatabaseContract.COLUMN_MOVIE_OVERVIEW).build())

    }
}