package mx.leo.testrappi.base.view

/**
 * Interface to handle loading view into interface
 */

interface LoadingView: View {
    fun showLoadingView()
    fun hideLoadingView()
}
