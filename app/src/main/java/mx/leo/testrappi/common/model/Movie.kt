package mx.leo.testrappi.common.model

/**
 * Created by mendezl on 15/05/2017.
 */
data class Movie(var poster_path:String?="",
                 var overview:String?="",
                 var release_date:String?="",
                 var id:String?="",
                 var original_title:String?="",
                 var vote_average:Double?=0.0)