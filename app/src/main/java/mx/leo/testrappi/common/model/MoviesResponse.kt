package mx.leo.testrappi.common.model

/**
 * Created by mendezl on 15/05/2017.
 */
data class MoviesResponse(var results:ArrayList<Movie>)