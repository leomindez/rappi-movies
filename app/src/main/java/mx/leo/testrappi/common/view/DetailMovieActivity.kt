package mx.leo.testrappi.common.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_detail_movie.*

import mx.leo.testrappi.R

class DetailMovieActivity : AppCompatActivity() {

    private val IMAGE_ENDPOINT = "http://image.tmdb.org/t/p/w300%s"

    companion object {
        public val MOVIE_DETAIL_TITLE = "movie_detail_title"
        val MOVIE_DETAIL_IMAGE = "movie_detail_image"
        val MOVIE_DETAIL_DESCRIPTION = "movie_detail_description"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_movie)

        showMovieDetail(intent?.getStringExtra(MOVIE_DETAIL_TITLE),
                intent?.getStringExtra(MOVIE_DETAIL_IMAGE),
                intent?.getStringExtra(MOVIE_DETAIL_DESCRIPTION))
    }

    private fun showMovieDetail(title:String?,image:String?,description:String?){
        movie_detail_img.setImageURI(String.format(IMAGE_ENDPOINT,image))
        movie_detail_title.setText(title)
        movie_detail_description.setText(description)
    }
}