package mx.leo.testrappi.common.view

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.View

import kotlinx.android.synthetic.main.activity_movies.*
import mx.leo.easyrecycler.util.RecyclerViewItemClickListener
import mx.leo.easyrecycler.util.extensions.onItemClickListener
import mx.leo.testrappi.R
import mx.leo.testrappi.base.data.net.NetManager
import mx.leo.testrappi.base.database.DatabaseManager
import mx.leo.testrappi.base.database.SqLiteHelper
import mx.leo.testrappi.common.model.Movie
import mx.leo.testrappi.common.view.adapter.MoviesAdapter
import mx.leo.testrappi.discover.data.usecase.MoviesDiscoveryUseCase
import mx.leo.testrappi.discover.presenter.MoviesDiscoveryPresenter
import mx.leo.testrappi.discover.view.MoviesDiscoveryView
import mx.leo.testrappi.rate.data.usecase.MoviesRatedUseCase
import mx.leo.testrappi.rate.presenter.MoviesRatedPresenter
import mx.leo.testrappi.rate.view.MoviesRatedView
import mx.leo.testrappi.top.data.usecase.MoviesTopUseCase
import mx.leo.testrappi.top.presenter.MoviesTopPresenter
import mx.leo.testrappi.top.view.MoviesTopView

class MoviesActivity : AppCompatActivity(),MoviesTopView, MoviesRatedView, MoviesDiscoveryView {


    private var moviesAdapter:MoviesAdapter
    private var moviesTopPresenter:MoviesTopPresenter? = null
    private var moviesRatePresenter:MoviesRatedPresenter? = null
    private var moviesDiscoveryPresenter:MoviesDiscoveryPresenter? = null
    private var databaseManager:DatabaseManager? = null

    init {
        databaseManager = DatabaseManager.instance.value
        databaseManager?.sqLiteDatabase = SqLiteHelper(this)

        setConfigNetManager()
        moviesAdapter = MoviesAdapter()
        moviesTopPresenter = MoviesTopPresenter(MoviesTopUseCase(NetManager.instance.value),databaseManager)
        moviesRatePresenter = MoviesRatedPresenter(MoviesRatedUseCase(NetManager.instance.value),databaseManager)
        moviesDiscoveryPresenter = MoviesDiscoveryPresenter(MoviesDiscoveryUseCase(NetManager.instance.value),databaseManager)

    }



    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                moviesTopPresenter?.showMoviesTop(isThereInternet())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_dashboard -> {
                moviesRatePresenter?.showRatedMovies(isThereInternet())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_notifications -> {
                moviesDiscoveryPresenter?.showMoviesDiscovery(isThereInternet())
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movies)

        val navigation = findViewById(R.id.navigation) as BottomNavigationView
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        movies.layoutManager = GridLayoutManager(this,2)
        movies.adapter = moviesAdapter
        movies.setHasFixedSize(true)
        movies.onItemClickListener(object: RecyclerViewItemClickListener.OnItemClickListener{
            override fun onItemClick(view: View?, position: Int?) {
                val movieDetailIntent = Intent(baseContext,DetailMovieActivity::class.java)
                val movie = moviesAdapter.items.get(position!!)
                movieDetailIntent.putExtra(DetailMovieActivity.MOVIE_DETAIL_TITLE,movie.original_title)
                movieDetailIntent.putExtra(DetailMovieActivity.MOVIE_DETAIL_IMAGE,movie.poster_path)
                movieDetailIntent.putExtra(DetailMovieActivity.MOVIE_DETAIL_DESCRIPTION,movie.overview)
                startActivity(movieDetailIntent)
            }
        })

        moviesTopPresenter?.showMoviesTop(isThereInternet())
        moviesTopPresenter?.view = this
        moviesRatePresenter?.view = this
        moviesDiscoveryPresenter?.view = this
    }


    override fun showMoviesTop(movies: ArrayList<Movie>) {
        moviesAdapter.addItems(movies)
    }

    override fun showMoviesRated(moviesRated: ArrayList<Movie>) {
        moviesAdapter.addItems(moviesRated)
    }

    override fun showMoviesDiscovery(moviesDiscovery: ArrayList<Movie>) {
        moviesAdapter.addItems(moviesDiscovery)
    }

    private fun setConfigNetManager(){
        NetManager.Config.baseUrl = "https://api.themoviedb.org/3"
        NetManager.Config.baseParams = listOf("api_key" to "8fc247525e4dfebc607c6ff20f551d54")
        NetManager.Config.headers = mapOf("Content-Type" to "application/json","charset" to "utf-8")

    }

    private fun isThereInternet(): Boolean {
        val connectionManager =  (this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)
        return if(connectionManager.activeNetworkInfo != null) connectionManager.activeNetworkInfo.isConnectedOrConnecting else false
    }

}
