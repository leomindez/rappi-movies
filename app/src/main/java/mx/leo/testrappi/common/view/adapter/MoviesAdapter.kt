package mx.leo.testrappi.common.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import mx.leo.easyrecycler.adapter.EasyAdapter
import mx.leo.testrappi.R
import mx.leo.testrappi.common.model.Movie

/**
 * Created by mendezl on 15/05/2017.
 */
class MoviesAdapter:EasyAdapter<MoviesViewHolder, Movie>() {

    override fun createHolder(parent: ViewGroup?, viewType: Int): MoviesViewHolder
    = MoviesViewHolder(LayoutInflater.from(parent?.context).
            inflate(R.layout.movie_item_layout,parent,false))

    override fun onBindItemViewHolder(holder: MoviesViewHolder, item: Movie, position: Int) {
        holder.bindItem(item)
    }
}