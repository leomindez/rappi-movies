package mx.leo.testrappi.common.view.adapter

import android.view.View
import kotlinx.android.synthetic.main.movie_item_layout.view.*
import mx.leo.easyrecycler.viewholder.EasyViewHolder
import mx.leo.testrappi.common.model.Movie

/**
 * Created by mendezl on 15/05/2017.
 */
class MoviesViewHolder(view: View):EasyViewHolder(view) {
    val BASE_URL_IMAGE = "http://image.tmdb.org/t/p/w185%s"

    fun bindItem(movie: Movie){
        itemView.movie_image.setImageURI(String.format(BASE_URL_IMAGE,movie.poster_path))
        itemView.movie_title.setText(movie.original_title)
    }
}