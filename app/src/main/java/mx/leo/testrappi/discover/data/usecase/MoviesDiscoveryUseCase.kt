package mx.leo.testrappi.discover.data.usecase

import mx.leo.testrappi.base.data.net.NetManager
import mx.leo.testrappi.base.usecase.UseCase
import mx.leo.testrappi.common.model.MoviesResponse

/**
 * Created by mendezl on 15/05/2017.
 */
class MoviesDiscoveryUseCase(private var netManager: NetManager):UseCase<List<Pair<String,String>>,MoviesResponse>() {
    private val MOVIES_DISCOVERY_ENDPOINT = "discover/movie"
    override fun execute(params: List<Pair<String, String>>?, success: (MoviesResponse) -> Unit, error: (Error) -> Unit) {
        netManager.get<MoviesResponse>(MOVIES_DISCOVERY_ENDPOINT,
                params,
                success = {success(it)},
                error = {error(it)})
    }
}