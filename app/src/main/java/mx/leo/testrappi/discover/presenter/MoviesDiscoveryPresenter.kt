package mx.leo.testrappi.discover.presenter

import mx.leo.testrappi.base.database.DatabaseContract
import mx.leo.testrappi.base.database.DatabaseManager
import mx.leo.testrappi.base.presenter.Presenter
import mx.leo.testrappi.discover.data.usecase.MoviesDiscoveryUseCase
import mx.leo.testrappi.discover.view.MoviesDiscoveryView

/**
 * Created by mendezl on 15/05/2017.
 */
class MoviesDiscoveryPresenter(private var useCase:MoviesDiscoveryUseCase, private var databaseManager: DatabaseManager?):Presenter<MoviesDiscoveryView>() {
    fun showMoviesDiscovery(isThereInternet:Boolean){
        if(isThereInternet) {
            val params = listOf("primary_release_date.gte" to "2017-05-17",
                    "primary_release_date.lte" to "2017-10-17")
            useCase.execute(params,
                    success = {
                        view?.showMoviesDiscovery(it.results)
                        databaseManager?.saveMovies(DatabaseContract.TABLE_DISCOVERY_MOVIES, it.results)
                    },
                    error = {})
        }else{
            databaseManager?.getMovies(DatabaseContract.TABLE_DISCOVERY_MOVIES,{
                movies -> view?.showMoviesDiscovery(movies)
            })
        }

    }
}