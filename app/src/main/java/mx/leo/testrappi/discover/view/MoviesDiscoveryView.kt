package mx.leo.testrappi.discover.view

import mx.leo.testrappi.base.view.View
import mx.leo.testrappi.common.model.Movie

/**
 * Created by mendezl on 15/05/2017.
 */
interface MoviesDiscoveryView : View {
    fun showMoviesDiscovery(moviesDiscovery:ArrayList<Movie>)
}