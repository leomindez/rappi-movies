package mx.leo.testrappi.rate.data.usecase

import mx.leo.testrappi.base.data.net.NetManager
import mx.leo.testrappi.base.usecase.UseCase
import mx.leo.testrappi.common.model.MoviesResponse

/**
 * Created by mendezl on 15/05/2017.
 */
class MoviesRatedUseCase(private var netManager: NetManager):UseCase<List<Pair<String,String>>,MoviesResponse>() {
    val MOVIES_RATED_ENDPOINT = "discover/movie"
    override fun execute(params: List<Pair<String, String>>?, success: (MoviesResponse) -> Unit, error: (Error) -> Unit) {
        netManager.get<MoviesResponse>(MOVIES_RATED_ENDPOINT,
                params,
                success = { success(it)},
                error = {error(it)})
    }
}