package mx.leo.testrappi.rate.presenter

import mx.leo.testrappi.base.database.DatabaseContract
import mx.leo.testrappi.base.database.DatabaseManager
import mx.leo.testrappi.base.presenter.Presenter
import mx.leo.testrappi.rate.data.usecase.MoviesRatedUseCase
import mx.leo.testrappi.rate.view.MoviesRatedView

/**
 * Created by mendezl on 15/05/2017.
 */
class MoviesRatedPresenter(private var useCase: MoviesRatedUseCase, private var databaseManager: DatabaseManager?):Presenter<MoviesRatedView>() {

    fun showRatedMovies(isThereInternet:Boolean){
        if(isThereInternet) {
            val params = listOf("certification_country" to "MX",
                    "certification" to "R",
                    "sort_by" to "vote_average.desc")

            useCase.execute(params,
                    success = {
                        view?.showMoviesRated(it.results)
                        databaseManager?.saveMovies(DatabaseContract.TABLE_RATED_MOVIES, it.results)
                    },
                    error = {})
        }else{
            databaseManager?.getMovies(DatabaseContract.TABLE_RATED_MOVIES,{
                movies -> view?.showMoviesRated(movies)
            })
        }

    }
}