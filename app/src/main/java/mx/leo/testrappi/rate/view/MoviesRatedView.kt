package mx.leo.testrappi.rate.view

import mx.leo.testrappi.base.view.View
import mx.leo.testrappi.common.model.Movie


/**
 * Created by mendezl on 15/05/2017.
 */
interface MoviesRatedView: View {
    fun showMoviesRated(moviesRated:ArrayList<Movie>)
}