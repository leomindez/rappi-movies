package mx.leo.testrappi.top.data.usecase

import mx.leo.testrappi.base.data.net.NetManager
import mx.leo.testrappi.base.usecase.UseCase
import mx.leo.testrappi.common.model.MoviesResponse

/**
 * Created by mendezl on 15/05/2017.
 */
class MoviesTopUseCase(private var netManager: NetManager?):UseCase<List<Pair<String,String>>, MoviesResponse>() {
     val MOVIES_TOP_ENDPOINT = "discover/movie"
    override fun execute(params: List<Pair<String,String>>?, success: (MoviesResponse) -> Unit, error: (Error) -> Unit) {
        netManager?.get<MoviesResponse>(MOVIES_TOP_ENDPOINT,params,
                success = { responseSuccess -> success(responseSuccess)},
                error = { responseError -> error(responseError)})
    }
}