package mx.leo.testrappi.top.presenter

import mx.leo.testrappi.base.database.DatabaseContract
import mx.leo.testrappi.base.database.DatabaseManager
import mx.leo.testrappi.base.presenter.Presenter
import mx.leo.testrappi.base.view.View
import mx.leo.testrappi.top.data.usecase.MoviesTopUseCase
import mx.leo.testrappi.top.view.MoviesTopView

/**
 * Created by mendezl on 15/05/2017.
 */
class MoviesTopPresenter(private var useCase:MoviesTopUseCase, private var databaseManager: DatabaseManager?):Presenter<MoviesTopView>() {

    fun showMoviesTop(isThereInternet:Boolean){
        if(isThereInternet) {
            val params = listOf("sort_by" to "popularity.desc")
            useCase.execute(params, success = {
                view?.showMoviesTop(it.results)
                databaseManager?.saveMovies(DatabaseContract.TABLE_TOP_MOVIES, it.results)
            }, error = {
            })
        }else{
            databaseManager?.getMovies(DatabaseContract.TABLE_TOP_MOVIES,{
                movies -> view?.showMoviesTop(movies)
            })
        }
    }

}