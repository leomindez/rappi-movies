package mx.leo.testrappi.top.view

import mx.leo.testrappi.base.view.LoadingView
import mx.leo.testrappi.base.view.View
import mx.leo.testrappi.common.model.Movie

/**
 * Created by mendezl on 15/05/2017.
 */
interface MoviesTopView: View {
    fun showMoviesTop(movies:ArrayList<Movie>)
}